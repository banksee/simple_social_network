from unittest import mock

import pytest
from django import forms

from apps.accounts.validators import EmailHunterValidator


@mock.patch('apps.accounts.validators.requests.get')
def test_email_hunter_validator_valid(mock_get):
    mock_get.return_value.ok = True
    mock_get.return_value.json = mock.Mock(return_value={
        "data": {"result": "deliverable"}
    })
    validator = EmailHunterValidator()

    test_email = "test@mail.com"
    result_email = validator(test_email)
    assert result_email == test_email, "Validator didn't return the test email"


@mock.patch('apps.accounts.validators.requests.get')
def test_email_hunter_validator_invalid(mock_get):
    mock_get.return_value.ok = True
    mock_get.return_value.json = mock.Mock(return_value={
        "data": {"result": "risky"}
    })
    validator = EmailHunterValidator()

    with pytest.raises(forms.ValidationError):
        validator("test@mail.co.uk")
