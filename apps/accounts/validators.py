import logging
from django import forms
from django.conf import settings

import requests
from django.core.cache import cache
from django.utils.deconstruct import deconstructible
from django.utils.translation import ugettext_lazy as _


logger = logging.getLogger(__name__)


@deconstructible
class EmailHunterValidator(object):
    """
    Hunter.io email validator that uses cache.
    """
    _hunter_url = settings.EMAILHUNTER_VERIFIER_ENDPOINT
    _hunter_key = settings.EMAILHUNTER_API_KEY
    _cache_prefix = settings.EMAIL_VALIDATION_CACHE_PREFIX

    def run_hunter_request(self, email):
        url = self._hunter_url
        params = ("?email={email}&api_key={key}"
                  .format(email=email, key=self._hunter_key))
        response = requests.get(url + params)

        if response.ok:
            resp_data = response.json()
            result = resp_data["data"]["result"]
        else:
            logger.error("Email verification failed:"
                         "\nStatus Code: {}\nError:{}"
                         .format(response.status_code, response.content))

            raise forms.ValidationError(
                _("Sorry, we are unable to register your account. "
                  "Please, try again later.")
            )

        return result

    def get_cached_result(self, email):
        key = self._build_cache_key(email)
        result = cache.get(key)
        return result

    def set_cached_result(self, email, result):
        key = self._build_cache_key(email)
        cache.set(key, result)

    def _build_cache_key(self, email):
        return self._cache_prefix + email

    def __call__(self, email):
        result = self.get_cached_result(email)

        if not result:
            result = self.run_hunter_request(email)
            self.set_cached_result(email, result)

        if result != "deliverable":
            raise forms.ValidationError(
                _("You are unable to use this email address. "
                  "Please, try another one.")
            )

        return email
