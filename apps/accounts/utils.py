import clearbit


def get_clearbit_data(email):
    """
    Returns information from Clearbit API for the given email.
    """
    return clearbit.Enrichment.find(
        email=email, stream=True
    )
