from rest_framework import mixins
from rest_framework import permissions
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework_jwt.views import (
    ObtainJSONWebToken as OriginalObtainJSONWebToken
)

from apps.accounts.models import User
from apps.posts.models import Post

from . import serializers


class ObtainJSONWebToken(OriginalObtainJSONWebToken):
    """
    API Endpoint that receives a POST with a user's username and password.

    Returns a JSON Web Token that can be used for authenticated requests.
    """
    pass


class UserRegisterViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
    API Endpoint for registering a user.
    """
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = (permissions.AllowAny,)


class PostViewSet(mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    """
    API Endpoint for creating posts.
    """
    queryset = Post.objects.all()
    serializer_class = serializers.PostSerializer

    @detail_route(methods=["POST", ],
                  serializer_class=serializers.PostShortSerializer)
    def like(self, request, *args, **kwargs):
        """
        API Endpoint for adding a like to certain post.
        """
        post = self.get_object()
        post.add_like(request.user)

        return Response({"success": True})

    @detail_route(methods=["POST", ],
                  serializer_class=serializers.PostShortSerializer)
    def unlike(self, request, *args, **kwargs):
        """
        API Endpoint for removing a like from certain post.
        """
        post = self.get_object()
        post.remove_like(request.user)

        return Response({"success": True})

    def get_serializer(self, *args, **kwargs):
        kwargs.update(user=self.request.user)
        return super(PostViewSet, self).get_serializer(*args, **kwargs)
