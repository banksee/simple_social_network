from rest_framework import serializers

from apps.accounts.models import User
from apps.posts.models import Post


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "email", "password",)
        extra_kwargs = {
            "id": {"read_only": True},
            "password": {"write_only": True},
        }

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)

        password = validated_data.get("password")
        user.set_password(password)
        user.save(update_fields=["password"])

        return user


class PostSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user")
        super(PostSerializer, self).__init__(*args, **kwargs)
        self.user = user

    class Meta:
        model = Post
        fields = ("id", "user", "title", "content", "likes", "created_at",)
        extra_kwargs = {
            "id": {"read_only": True},
            "user": {"read_only": True},
            "likes": {"read_only": True},
        }

    def create(self, validated_data):
        validated_data.update(user=self.user)
        return super(PostSerializer, self).create(validated_data)


class PostShortSerializer(PostSerializer):
    class Meta:
        model = Post
        fields = ("id",)
