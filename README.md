## Simple Social Network ##

Implementation of simple social network RESTful API with JWT authorization.

### Features ###

* User registration using [Clearbit Enrichment](https://clearbit.com/enrichment) for fetching additional user information and [hunter.io](https://hunter.io) for email validation.
* User authorization (using JWT)
* User post creation
* Post like
* Post unlike

### Usage ###

* Setup and activate the `virtualenv` 
* Install dependencies `pip install -r requirements.txt` 
* Apply migrations `./manage.py migrate`
* Set the environment variable for Clearbit API key: `export CLEARBIT_KEY=your_clearbit_key`
* Set the environment variable for Hunter.io API key: `export EMAILHUNTER_API_KEY=your_hunter_io_key`
* Run the server `./manage.py runserver`


### Tech Stack ###
* Python 3.5
* Django 1.10
* Django Rest Framework 3.5.4
* SQLite3 database